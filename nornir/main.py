#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-


"""
Description ...

"""

__author__ = "Dylan Hamel"
__maintainer__ = "Dylan Hamel"
__version__ = "1.0"
__email__ = "dylan.hamel@protonmail.com"
__status__ = "Prototype"
__copyright__ = "Copyright 2019"

######################################################
#
# Default value used for exit()
#
EXIT_SUCCESS = 0
EXIT_FAILURE = 1

######################################################
#
# Import Library
#

#
# Own libraries
#
try:
    import netests.pings
except ImportError as importError:
    print("Error import [main] netests.pings")
    print(importError)
    exit(EXIT_FAILURE)

try:
    import netests.lldp
except ImportError as importError:
    print("Error import [main] netests.lldp")
    print(importError)
    exit(EXIT_FAILURE)

try:
    import netests.bgp
except ImportError as importError:
    print("Error import [main] netests.bgp")
    print(importError)
    exit(EXIT_FAILURE)

#
# Data Structure
#
try:
    import json
except ImportError as importError:
    print("Error import [main] json")
    print(importError)
    exit(EXIT_FAILURE)

try:
    import yaml
except ImportError as importError:
    print("Error import [main] yaml")
    print(importError)
    exit(EXIT_FAILURE)

try:
    import textfsm
except ImportError as importError:
    print("Error import [main] textfsm")
    print(importError)
    exit(EXIT_FAILURE)

#
# WEB SERVER
#
try:
    from flask import Flask, Response, request
except ImportError as importError:
    print("Error import [main] flask")
    print(importError)
    exit(EXIT_FAILURE)

#
# Network Libraries
#
try:
    import netmiko

except ImportError as importError:
    print("Error import [main] netmiko")
    print(importError)
    exit(EXIT_FAILURE)

try:
    import pprint
    PP = pprint.PrettyPrinter(indent=4)
except ImportError as importError:
    print("Error import [main] pprint")
    print(importError)
    exit(EXIT_FAILURE)

try:
    from nornir import InitNornir
    from nornir.core.task import Result

# To use advanced filters
    from nornir.core.filter import F

# To use HTTP requests
    from nornir.plugins.tasks.apis import http_method

# To execute commands through SSH
    from nornir.plugins.tasks.commands import remote_command

# Netmiko commands
    from nornir.plugins.tasks.networking import netmiko_send_command
    from nornir.plugins.tasks.networking import netmiko_save_config
    from nornir.plugins.tasks.networking import netmiko_send_config

# To use PING
    from nornir.plugins.tasks.networking import tcp_ping

# To print task results
    from nornir.plugins.functions.text import print_result

# To retrieve device datas in a YAML file
    from nornir.plugins.tasks.data import load_yaml

# To generate template from Jinja2
    from nornir.plugins.tasks.text import template_file

# To raise Nornir exceptions
    from nornir.core.exceptions import CommandError
    from nornir.core.exceptions import NornirExecutionError
    from nornir.core.exceptions import NornirSubTaskError

except ImportError as importError:
    print("Error import [main] nornir")
    print(importError)
    exit(EXIT_FAILURE)


######################################################
#
# Constantes
#
LLDP_LINKS_TO_CHECK = "./topology/lldp_links.yml"
PING_HOST_TO_CHECK = "./topology/ping_network.yml"
BGP_SESSIONS_TO_CHECK = "./topology/bgp_sessions.yml"

######################################################
#
# Variables
#
nr = InitNornir(
    config_file="./nornir/config.yml",
    logging={"file": "./nornir/nornir.log", "level": "debug"}
)

######################################################
#
# Functions
#


######################################################
#
# MAIN Functions
#
def main():
    
    devices = nr.filter(F(groups__contains="spine"))
    print(devices.inventory.hosts)
    
    file = open_file(PING_HOST_TO_CHECK)

    result = devices.run(
        task=netests.pings._generate_ping_command,
        file=file,
        num_workers=10
    )

    result = devices.run(
        task=netests.pings._ping_network_devices,
        num_workers=10
    )

    print_result(result)



######################################################
#
# Other functions
#
def open_file(path: str()) -> dict():
    """
    This function  will open a yaml file and return is data

    Args:
        param1 (str): Path to the yaml file

    Returns:
        str: Node name
    """

    with open(path, 'r') as yamlFile:
        try:
            data = yaml.load(yamlFile)
        except yaml.YAMLError as exc:
            print(exc)

    return data
######################################################
#
# WEB API
#
app = Flask(__name__)


@app.route("/", methods=['GET'])
def index():
    rep = str(
        "/get_cumulus_bgp_summary" + "</br>" +
        "/check_cumulus_bgp_established" + "</br>" +
        "/check_cumulus_bgp" + "</br>" +
        "/get_cumulus_lldp" + "</br>" +
        "/get_cumulus_lldp_cli" + "</br>" +
        "/check_cumulus_lldp" + "</br>" +
        "/ping_network_devices" + "</br>"
    )

    return Response(
        rep,
        status=200,
        mimetype="text/html"
    )


@app.route("/check_cumulus_bgp_established", methods=['GET'])
def check_cumulus_bgp_established():

    path_url = request.args.get('path')

    if path_url is None:
        path_url = BGP_SESSIONS_TO_CHECK

    role_url = request.args.get('role')

    if role_url is not None:
        devices = nr.filter(role=role_url)
    else:
        devices = nr.filter(all="all")

    print(devices.inventory.hosts)

    data = devices.run(
        task=netests.bgp._cumulus_get_bgp_summary,
        on_failed=True,
        num_workers=10
    )

    netests.bgp._cumulus_bgp_summary_converter(devices)

    content = open_file(path_url)
    content = (content)

    result = dict()

    result['bgp_sessions_ok'] = netests.bgp.check_all_bgp_sessions_established(devices)


    return Response(
        json.dumps(result),
        status=200,
        mimetype="application/json"
    )










@app.route("/ping_network_devices", methods=['GET'])
def ping_network_devices():
    """
    /ping_network_devices endpoint

    http://127.0.0.1/ping_network_devices
   
    """

    devices = nr.filter(role="leaf")
    file = open_file(PING_HOST_TO_CHECK)

    data = devices.run(
        task=netests.pings._generate_ping_command,
        file=file,
        num_workers=10
    )

    data = devices.run(
        task=netests.pings._ping_network_devices,
        num_workers=10
    )
    
    result = dict()
    result['pings_ok'] = not data.failed

    for h_failed in data.failed_hosts:
        print(h_failed)
        result[h_failed] = "Failed"

    return Response(
        json.dumps(result),
        status=200,
        mimetype="application/json"
    )


@app.route("/get_cumulus_bgp_summary", methods=['GET'])
def get_cumulus_bgp_summary():
    """
    /get_cumulus_bgp_summary endpoint

    http://127.0.0.1/get_cumulus_bgp_summary?role=spine
    http://127.0.0.1/get_cumulus_bgp_summary?role=leaf
    http://127.0.0.1/get_cumulus_bgp_summary
   
    """
    role_url = request.args.get('role')

    if role_url is not None:
        devices = nr.filter(role=role_url)
    else:
        devices = nr.filter(all="all")

    print(devices.inventory.hosts)

    data = devices.run(
        task=netests.bgp._cumulus_get_bgp_summary,
        on_failed=True,
        num_workers=10
    )
    print_result(data)

    netests.bgp._cumulus_bgp_summary_converter(devices)

    result = dict()
    for host in devices.inventory.hosts:
        result[host] = devices.inventory.hosts[host]["bgp_brief"]

    return Response(
        json.dumps(result),
        status=200,
        mimetype="application/json"
    )


@app.route("/check_cumulus_bgp", methods=['GET'])
def check_cumulus_bgp():

    path_url = request.args.get('path')

    if path_url is None:
        path_url = BGP_SESSIONS_TO_CHECK

    role_url = request.args.get('role')

    if role_url is not None:
        devices = nr.filter(role=role_url)
    else:
        devices = nr.filter(all="all")

    print(devices.inventory.hosts)

    data = devices.run(
        task=netests.bgp._cumulus_get_bgp_summary,
        on_failed=True,
        num_workers=10
    )
    #print_result(data)

    netests.bgp._cumulus_bgp_summary_converter(devices)

    content = open_file(path_url)
    content = (content)

    result = dict()

    result['bgp_sessions_ok'] = netests.bgp.compare_topology_and_bgp_output(
        content, devices)

    for h_failed in data.failed_hosts:
        print(h_failed)
        result[h_failed] = "Failed"

    return Response(
        json.dumps(result),
        status=200,
        mimetype="application/json"
    )



@app.route("/get_cumulus_lldp", methods=['GET'])
def get_cumulus_lldp():
    """
    /get_cumulus_lldp endpoint

    http://127.0.0.1/get_cumulus_lldp?role=spine
    http://127.0.0.1/get_cumulus_lldp?role=leaf
    http://127.0.0.1/get_cumulus_lldp
   
    """
    role_url = request.args.get('role')

    if role_url is not None:
        devices = nr.filter(role=role_url)
    else:
        devices = nr.filter(all="all")


    print(devices.inventory.hosts)

    data = devices.run(
        task=netests.lldp._cumulus_get_lldp_neighbors,
        on_failed=True,
        num_workers=10
    )
    print_result(data)

    netests.lldp._cumulus_lldp_converter(devices)

    result = dict()
    for host in devices.inventory.hosts:
        result[host] = devices.inventory.hosts[host]["lldp_brief"]

    return Response(
        json.dumps(result),
        status=200,
        mimetype="application/json"
    )

@app.route("/get_cumulus_lldp_cli", methods=['GET'])
def get_cumulus_lldp_cli():
    """
    /get_cumulus_lldp endpoint

    http://127.0.0.1/get_cumulus_lldp_cli?role=spine
    http://127.0.0.1/get_cumulus_lldp_cli?role=leaf
    http://127.0.0.1/get_cumulus_lldp_cli
   
    """
    role_url = request.args.get('role')

    if role_url is not None:
        devices = nr.filter(role=role_url)
    else:
        devices = nr.filter(all="all")

    data = devices.run(
        task=netests.lldp._cumulus_get_lldp_neighbors_cli,
        on_failed=True,
        num_workers=10
    )
    print_result(data)

    result = str()

    for host in devices.inventory.hosts:
        text = devices.inventory.hosts[host]['lldp']
        text = text.replace("\n", "</br>")
        result = result + text + "</br>"
    
    return Response(
        result,
        status=200,
        mimetype="text/html"
    )


@app.route("/check_cumulus_lldp", methods=['GET'])
def check_cumulus_lldp():
    
    path_url = request.args.get('path')
    if path_url is None:
        path_url = LLDP_LINKS_TO_CHECK

    devices = nr.filter(all="all")

    data = devices.run(
        task=netests.lldp._cumulus_get_lldp_neighbors,
        on_failed=True,
        num_workers=10
    )

    netests.lldp._cumulus_lldp_converter(devices)

    content = open_file(path_url)
    content = netests.lldp.retrieve_topology_from_architecture(content)

    result = dict()

    result['lldp_links_ok'] = netests.lldp.compare_topology_and_lldp_output(
        content, devices)

    for h_failed in data.failed_hosts:
        print(h_failed)
        result[h_failed] = "Failed"

    return Response(
        json.dumps(result),
        status=200,
        mimetype="application/json"
    )


# -----------------------------------------------------------------------------------------------------------------------------
#
#
if __name__ == "__main__":
    #main()
    app.run(host="0.0.0.0", port=55555, debug=True)
