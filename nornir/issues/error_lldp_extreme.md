# Error LLDP - Extreme Network and Cumulus

If a Extreme Network VSP device is directly connected to a Cumulus Network device. There is a error in ``net show lldp neighbors`` output command :

![error_lldp_extreme_01](./../images/error_lldp_extreme_01.png)

RemotePort = "Extreme Networks Virtual Service Platform 8284XSQ".
It's an error !

For avoid this error the following condition has been added :

```python
for d in data['lldp'][0]['interface']:
  ##
  ## For the following condition => Check ./issues/error_lldp_extreme.md
  ##
  if LLDP_NEIGHBOR_EXTREME not in d['port'][0]['descr'][0]['value']:
    resultLLDP[d['name']] = dict()
    ...
    ...
```

JSON output :

```json
ok: [Exit02] =>
  lldp_output["msg"]:
    lldp:
    - interface:
      - age: 0 day, 01:02:20
        chassis:
        - capability:
          - enabled: false
            type: Bridge
          - enabled: true
            type: Router
          descr:
          - value: Cumulus Linux version 3.7.5 running on Bochs Bochs
          id:
          - type: mac
            value: '50:00:00:01:00:00'
          mgmt-ip:
          - value: 10.10.0.101
          - value: fe80::5200:ff:fe01:0
          name:
          - value: Spine01
        lldp-med:
        - capability:
          - available: true
            type: Capabilities
          - available: true
            type: Policy
          - available: true
            type: Location
          - available: true
            type: MDI/PSE
          - available: true
            type: MDI/PD
          - available: true
            type: Inventory
          device-type:
          - value: Network Connectivity Device
          inventory:
          - firmware:
            - value: Bochs
            manufacturer:
            - value: Bochs
            model:
            - value: Bochs
            serial:
            - value: Not Specified
            software:
            - value: 3.7.5
        name: swp1
        port:
        - auto-negotiation:
          - current:
            - value: 1000BaseTFD - Four-pair Category 5 UTP, full duplex mode
            enabled: false
            supported: false
          descr:
          - value: swp6
          id:
          - type: ifname
            value: swp6
          ttl:
          - value: '120'
        rid: '7'
        via: LLDP
      - age: 0 day, 01:02:21
        chassis:
        - capability:
          - enabled: false
            type: Bridge
          - enabled: true
            type: Router
          descr:
          - value: Cumulus Linux version 3.7.5 running on Bochs Bochs
          id:
          - type: mac
            value: '50:00:00:02:00:00'
          mgmt-ip:
          - value: 10.10.0.102
          - value: fe80::5200:ff:fe02:0
          name:
          - value: Spine02
        lldp-med:
        - capability:
          - available: true
            type: Capabilities
          - available: true
            type: Policy
          - available: true
            type: Location
          - available: true
            type: MDI/PSE
          - available: true
            type: MDI/PD
          - available: true
            type: Inventory
          device-type:
          - value: Network Connectivity Device
          inventory:
          - firmware:
            - value: Bochs
            manufacturer:
            - value: Bochs
            model:
            - value: Bochs
            serial:
            - value: Not Specified
            software:
            - value: 3.7.5
        name: swp2
        port:
        - auto-negotiation:
          - current:
            - value: 1000BaseTFD - Four-pair Category 5 UTP, full duplex mode
            enabled: false
            supported: false
          descr:
          - value: swp6
          id:
          - type: ifname
            value: swp6
          ttl:
          - value: '120'
        rid: '6'
        via: LLDP
      - age: 0 day, 01:01:21
        chassis:
        - capability:
          - enabled: true
            type: Bridge
          - enabled: true
            type: Router
          descr:
          - value: Cumulus Linux version 3.7.5 running on Bochs Bochs
          id:
          - type: mac
            value: '50:00:00:07:00:00'
          mgmt-ip:
          - value: 10.10.0.151
          - value: fc00:14::6
          name:
          - value: Exit01
        lldp-med:
        - capability:
          - available: true
            type: Capabilities
          - available: true
            type: Policy
          - available: true
            type: Location
          - available: true
            type: MDI/PSE
          - available: true
            type: MDI/PD
          - available: true
            type: Inventory
          device-type:
          - value: Network Connectivity Device
          inventory:
          - firmware:
            - value: Bochs
            manufacturer:
            - value: Bochs
            model:
            - value: Bochs
            serial:
            - value: Not Specified
            software:
            - value: 3.7.5
        name: swp6
        port:
        - auto-negotiation:
          - current:
            - value: 1000BaseTFD - Four-pair Category 5 UTP, full duplex mode
            enabled: false
            supported: false
          descr:
          - value: swp6
          id:
          - type: ifname
            value: swp6
          ttl:
          - value: '120'
        rid: '8'
        via: LLDP
      - age: 0 day, 01:01:21
        chassis:
        - capability:
          - enabled: true
            type: Bridge
          - enabled: true
            type: Router
          descr:
          - value: Cumulus Linux version 3.7.5 running on Bochs Bochs
          id:
          - type: mac
            value: '50:00:00:07:00:00'
          mgmt-ip:
          - value: 10.10.0.151
          - value: fc00:14::6
          name:
          - value: Exit01
        lldp-med:
        - capability:
          - available: true
            type: Capabilities
          - available: true
            type: Policy
          - available: true
            type: Location
          - available: true
            type: MDI/PSE
          - available: true
            type: MDI/PD
          - available: true
            type: Inventory
          device-type:
          - value: Network Connectivity Device
          inventory:
          - firmware:
            - value: Bochs
            manufacturer:
            - value: Bochs
            model:
            - value: Bochs
            serial:
            - value: Not Specified
            software:
            - value: 3.7.5
        name: swp7
        port:
        - auto-negotiation:
          - current:
            - value: 1000BaseTFD - Four-pair Category 5 UTP, full duplex mode
            enabled: false
            supported: false
          descr:
          - value: swp7
          id:
          - type: ifname
            value: swp7
          ttl:
          - value: '120'
        rid: '8'
        via: LLDP
      - age: 0 day, 01:09:34
        chassis:
        - capability:
          - enabled: true
            type: Bridge
          - enabled: true
            type: Router
          descr:
          - value: VSP-8284XSQ (7.1.0.0)
          id:
          - type: mac
            value: 00:51:00:09:00:00
          mgmt-ip:
          - value: 10.0.5.51
          name:
          - value: core01a
        name: swp8
        port:
        - descr:
          - value: Extreme Networks Virtual Services Platform 8284XSQ - 10GbNone Port 1/2
          id:
          - type: ifname
            value: 1/2
          ttl:
          - value: '120'
        rid: '2'
        via: LLDP
      - age: 0 day, 01:09:19
        chassis:
        - capability:
          - enabled: true
            type: Bridge
          - enabled: true
            type: Router
          descr:
          - value: VSP-8284XSQ (7.1.0.0)
          id:
          - type: mac
            value: 00:51:00:0a:00:00
          mgmt-ip:
          - value: 10.0.4.52
          name:
          - value: core01b
        name: swp9
        port:
        - descr:
          - value: Extreme Networks Virtual Services Platform 8284XSQ - 10GbNone Port 1/2
          id:
          - type: ifname
            value: 1/2
          ttl:
          - value: '120'
        rid: '3'
        via: LLDP
```

