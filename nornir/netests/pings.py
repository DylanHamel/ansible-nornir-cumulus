#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-


"""
Description ...

"""

__author__ = "Dylan Hamel"
__maintainer__ = "Dylan Hamel"
__version__ = "1.0"
__email__ = "dylan.hamel@protonmail.com"
__status__ = "Prototype"
__copyright__ = "Copyright 2019"

######################################################
#
# Default value used for exit()
#
EXIT_SUCCESS = 0
EXIT_FAILURE = 1

######################################################
#
# Import Library
#
try:
    from nornir.plugins.tasks.text import template_file
    from nornir.plugins.tasks.commands import remote_command
    from nornir.core.exceptions import NornirSubTaskError
except ImportError as importError:
    print("Error import [pings.py] nornir")
    print(importError)
    exit(EXIT_FAILURE)

######################################################
#
# Constantes
#
PATH_JINJA2_TEMPLATE = "./templates/jinja2/"
PATH_RESULT_TEMPLATE = "./templates/files/"
RESULT_EXTENSIONS = "_ping.cfg"
PING_TEMPLATE = "ping.j2"
PINGS_KEY_FOR_NORNIR_HOSTS = "pings"

# ====================================================
# PING PART
# ====================================================

# ----------------------------------------------------
#
#
def _hosts_to_ping(file: dict(), hostname: str()) -> list():

    lst_hosts = list()

    for host in file[PINGS_KEY_FOR_NORNIR_HOSTS]['all']:
        lst_hosts.append(host)

    for host in file[PINGS_KEY_FOR_NORNIR_HOSTS][str(hostname)]:
        lst_hosts.append(host)

    return lst_hosts

# ----------------------------------------------------
#
#
def _ping_network_devices(task):

    file = open(
        f"{PATH_RESULT_TEMPLATE}{task.host.name}{RESULT_EXTENSIONS}", "r")

    for ping_line in file:
        try:
            output = task.run(
                name=f"Ping network devices",
                task=remote_command,
                command=ping_line
            )

        except NornirSubTaskError as identifier:
            print(type(output.failed))
            print(output.failed)
            print(identifier)

# ----------------------------------------------------
#
#
def _generate_ping_command(task, file):

    task.host[PINGS_KEY_FOR_NORNIR_HOSTS] = _hosts_to_ping(
        file, str(task.host))
    print(task.host[PINGS_KEY_FOR_NORNIR_HOSTS])

    output = task.run(
        name=f"Generate templates from {PATH_JINJA2_TEMPLATE}",
        task=template_file,
        template=f"{PING_TEMPLATE}",
        path=f"{PATH_JINJA2_TEMPLATE}"
    )

    print(output.result)

    file = open(f"{PATH_RESULT_TEMPLATE}{task.host.name}{RESULT_EXTENSIONS}", "w+")
    file.write(output.result)
    file.close()
