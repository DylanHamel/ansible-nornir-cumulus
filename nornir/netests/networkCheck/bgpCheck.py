#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-

"""
Description ...

"""

__author__ = "Dylan Hamel"
__maintainer__ = "Dylan Hamel"
__version__ = "0.1"
__email__ = "dylan.hamel@protonmail.com"
__status__ = "Prototype"
__copyright__ = "Copyright 2019"

######################################################
#
# Default value used for exit()
#
EXIT_SUCCESS = 0
EXIT_FAILURE = 1

######################################################
#
# Class
#
class BGPSession:

    src: str
    dst: str

    # ------------------------------------------------------------
    #
    #
    def __init__(self, src: str(),  dst: str()):
        self.src = src
        self.dst = dst

    # ------------------------------------------------------------
    #
    #
    def __eq__(self, other):
        if not isinstance(other, BGPSession):
            return NotImplemented
    
        return ((self.src == other.src and
                self.dst == other.dst) or 
                (self.src == other.dst and
                self.dst == other.src))

    # ------------------------------------------------------------
    #
    #
    def __repr__(self):
        return f"<BGPSession src={self.src} dst={self.dst}>\n"
