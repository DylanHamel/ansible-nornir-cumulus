#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-


"""
Description ...

"""

__author__ = "Dylan Hamel"
__maintainer__ = "Dylan Hamel"
__version__ = "1.0"
__email__ = "dylan.hamel@protonmail.com"
__status__ = "Prototype"
__copyright__ = "Copyright 2019"

######################################################
#
# Default value used for exit()
#
EXIT_SUCCESS = 0
EXIT_FAILURE = 1

######################################################
#
# Import Library
#
try:
    from nornir.plugins.tasks.networking import netmiko_send_command
    from nornir.core import Nornir
except ImportError as importError:
    print("Error import [lldp.py] nornir")
    print(importError)
    exit(EXIT_FAILURE)

try:
    import json

except ImportError as importError:
    print("Error import [lldp.py] json")
    print(importError)
    exit(EXIT_FAILURE)

try:
    from netests.networkCheck.lldpCheck import LLDPLink

except ImportError as importError:
    print(importError.msg)
    print("Error import [netests] LLDPLink")
    print(importError)
    exit(EXIT_FAILURE)

######################################################
#
# Constantes
#
LLDP_KEY_FOR_NORNIR_HOSTS = "lldp"
LLDP_NEIGHBOR_EXTREME = "Extreme Networks"

# ====================================================
# LLDP PART
# ====================================================

# ----------------------------------------------------
#
#
def _cumulus_lldp_converter(nornirObj):

    for host in nornirObj.inventory.hosts:
        resultLLDP = dict()
        data = json.loads(
            nornirObj.inventory.hosts[host][LLDP_KEY_FOR_NORNIR_HOSTS])
        
        if "interface" in data[LLDP_KEY_FOR_NORNIR_HOSTS][0].keys():
            for d in data[LLDP_KEY_FOR_NORNIR_HOSTS][0]['interface']:
                ##
                ## For the following condition => Check ./issues/error_lldp_extreme.md
                ##
                if LLDP_NEIGHBOR_EXTREME not in d['port'][0]['descr'][0]['value']:
                    resultLLDP[d['name']] = dict()

                    resultLLDP[d['name']
                            ]['mac'] = d['chassis'][0]['id'][0]['value']
                    resultLLDP[d['name']
                            ]['ipv4'] = d['chassis'][0]['mgmt-ip'][0]['value']
                    if len(d['chassis'][0]['mgmt-ip']) == 2:
                        resultLLDP[d['name']
                                ]['ipv6'] = d['chassis'][0]['mgmt-ip'][1]['value']

                    resultLLDP[d['name']
                            ]['neighbor'] = d['chassis'][0]['name'][0]['value']

                    resultLLDP[d['name']
                            ]['neighbor_port'] = d['port'][0]['id'][0]['value']

        nornirObj.inventory.hosts[host]["lldp_brief"] = resultLLDP

            


# ----------------------------------------------------
#
#
def _cumulus_get_lldp_neighbors(task):

    output = task.run(
        name="net show lldp json",
        task=netmiko_send_command,
        command_string="net show lldp json"
    )

    task.host[LLDP_KEY_FOR_NORNIR_HOSTS] = output.result


# ----------------------------------------------------
#
#
def _cumulus_get_lldp_neighbors_cli(task):

    output = task.run(
        name="net show lldp json",
        task=netmiko_send_command,
        command_string="net show lldp"
    )

    task.host[LLDP_KEY_FOR_NORNIR_HOSTS] = output.result

# ----------------------------------------------------
#
#
def retrieve_topology_from_architecture(data: dict()) -> dict():
    """
    This function will parse and retrieve informations based on Network Architecture.
    Extract physical connexions.
    """

    result = dict()

    i = 1
    for link in data['links']:
        if link['dst'] != "OOB-NETWORK":
            result[i] = dict()
            result[i]['src'] = link['src']
            result[i]['spt'] = link['sport']
            result[i]['dst'] = link['dst']
            result[i]['dpt'] = link['dport']
            i = i + 1

    return result

# ----------------------------------------------------
#
#
def compare_topology_and_lldp_output(topology: dict(), devices: Nornir) -> bool:

    for host in devices.inventory.hosts:
        temp = dict()
        temp[host] = devices.inventory.hosts[host]["lldp_brief"]
        if not _compare_topology_and_lldp_output(topology, temp):
            return False

    return True

# ----------------------------------------------------
#
#
def _compare_topology_and_lldp_output(topology: dict(), lldpOutput: dict()) -> bool:

    lst_local_config: list = list()
    lst_running_config: list = list()

    # Get local configs
    for x in topology.values():
        lldp_link = LLDPLink(src=x['src'], dst=x['dst'],
                             spt=x['spt'], dpt=x['dpt'])
        lst_local_config.append(lldp_link)

    # Remote configs
    for hostname, value in lldpOutput.items():
        for port, facts in value.items():
            remote_lldp_link = LLDPLink(
                src=hostname, spt=port, dst=facts['neighbor'], dpt=facts['neighbor_port'])
            lst_running_config.append(remote_lldp_link)

    # Compare
    #for lldp_link in lst_running_config:
    for lldp_link in lst_local_config:
        #if lldp_link not in lst_local_config:
        if lldp_link not in lst_running_config:
            print(f"lldp_link = {lldp_link}")
            print(f"lst_local_config = {lst_local_config}")
            return False

    return True
