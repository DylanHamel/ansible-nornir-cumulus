#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-


"""
Description ...

"""

__author__ = "Dylan Hamel"
__maintainer__ = "Dylan Hamel"
__version__ = "1.0"
__email__ = "dylan.hamel@protonmail.com"
__status__ = "Prototype"
__copyright__ = "Copyright 2019"

######################################################
#
# Default value used for exit()
#
EXIT_SUCCESS = 0
EXIT_FAILURE = 1

######################################################
#
# Import Library
#
try:
    from nornir.plugins.tasks.networking import netmiko_send_command
    from nornir.core import Nornir
except ImportError as importError:
    print("Error import [bgp.py] nornir")
    print(importError)
    exit(EXIT_FAILURE)

try:
    import json

except ImportError as importError:
    print("Error import [bgp.py] json")
    print(importError)
    exit(EXIT_FAILURE)

try:
    from netests.networkCheck.bgpCheck import BGPSession
except ImportError as importError:
    print(importError.msg)
    print("Error import [netests] BGPSession")
    print(importError)
    exit(EXIT_FAILURE)

######################################################
#
# Constantes
#
BGP_KEY_FOR_NORNIR_HOSTS = "bgp"
BGP_SESSION_ESTABLISHED = "Established"

# ====================================================
# BGP PART
# ====================================================

# ----------------------------------------------------
#
#
def _cumulus_get_bgp_summary(task):

    output = task.run(
        name="net show bgp summary json",
        task=netmiko_send_command,
        command_string="net show bgp summary json"
    )

    task.host[BGP_KEY_FOR_NORNIR_HOSTS] = output.result

# ----------------------------------------------------
#
#
def _cumulus_bgp_summary_converter(nornirObj):

    for host in nornirObj.inventory.hosts:
        resultBGP = dict()

        if BGP_KEY_FOR_NORNIR_HOSTS in nornirObj.inventory.hosts[host].keys():
            data = json.loads(
                nornirObj.inventory.hosts[host][BGP_KEY_FOR_NORNIR_HOSTS])
        else:
            raise Exception(
                "[_cumulus_bgp_summary_converter] - error with BGP key => check inventory hosts file")

        if "ipv4 unicast" in data.keys():
            if data['ipv4 unicast'] is None:
                resultBGP[host] = dict()
            else:
                resultBGP[host] = dict()
                resultBGP[host]['as'] = data['ipv4 unicast']['as']
                resultBGP[host]['routerId'] = data['ipv4 unicast']['routerId']
                resultBGP[host]['bestPath'] = "multiPathRelax " + \
                    data['ipv4 unicast']['bestPath']['multiPathRelax']

                resultBGP[host]['vrfName'] = data['ipv4 unicast']['vrfName']
                resultBGP[host]['totalPeers'] = data['ipv4 unicast']['totalPeers']
                resultBGP[host]['interfaces'] = dict()
                for port, facts in data['ipv4 unicast']['peers'].items():
                    resultBGP[host]['interfaces'][port] = dict()
                    if 'hostname' in facts.keys():
                        resultBGP[host]['interfaces'][port]['bgp_neighbor'] = facts['hostname']
                    else:
                        resultBGP[host]['interfaces'][port]['bgp_neighbor'] = "None"
                    resultBGP[host]['interfaces'][port]['peerUptime'] = facts['peerUptime']
                    resultBGP[host]['interfaces'][port]['state'] = facts['state']

        #if "ipv6 unicast" in data.keys():
        #    if data['ipv6 unicast'] is None:
        #        resultBGP[host]['ipv6_unicast'] = dict()
        #    else:
        #        pass

        #if "l2vpn evpn" in data.keys():
        #    if data['l2vpn evpn'] is None:
        #        resultBGP[host]['l2vpn_evpn'] = dict()
        #    else:

        nornirObj.inventory.hosts[host]["bgp_brief"] = resultBGP[host]

# ----------------------------------------------------
#
#
def compare_topology_and_bgp_output(topology: dict(), devices: Nornir) -> bool:

    lst_running_bgp_sessions = list()

    # Get running configuration
    for host in devices.inventory.hosts:
        for facts in devices.inventory.hosts[host]['bgp_brief']['interfaces'].values():
            bgp_sessions = BGPSession(src=host, dst=facts['bgp_neighbor'])
            lst_running_bgp_sessions.append(bgp_sessions)

    for bgp_session in topology['bgp_sessions']:
        # bgp_session['host'] = device hostname
        # bgp_session['neighbors'] = neighbors list

        for neighbor in bgp_session['neighbors']:
            bgp = BGPSession(src=bgp_session['host'], dst=neighbor)
            if bgp not in lst_running_bgp_sessions:
                return False

    return True


# ----------------------------------------------------
#
#
def check_all_bgp_sessions_established(devices: Nornir) -> bool:

    lst_running_bgp_sessions = list()

    # Get running configuration
    for host in devices.inventory.hosts:
        for facts in devices.inventory.hosts[host]['bgp_brief']['interfaces'].values():
            if facts['state'] != BGP_SESSION_ESTABLISHED:
                return False

    return True
