# Test your Network with Nornir

###### Dylan Hamel - <dylan.hamel@protonmail.com>



## Run HTTP server

```bash
» ./main.py 
```

![index.png](./images/index.png)

## Test ping

In a YAML file ``./topology/ping_network.yml``, define IPv4 adresse that each host has to be able to reach

``all:`` => All hosts have to be able to reach these IPv4 adresses.

```yaml
pings:
  all:
    - 10.10.0.101
    - 10.10.0.102
    - 10.10.0.201
    - 10.10.0.202
    - 10.10.0.203
    - 10.10.0.204

  Spine01:
    - 10.0.4.101

  Spine02:
    - 10.0.4.102

  Leaf01:
    - 10.0.4.201
    # IP below is unreachable
    - 10.0.4.111

  Leaf02:
    - 10.0.4.202

  Leaf03:
    - 10.0.4.203

  Leaf04:
    - 10.0.4.204
```

![check_ping.png](./images/check_pings.png)



## Check BGP sessions:

In a YAML file ``./topology/bgp_sessions.yml``, define BGP sessions between devices.

```yaml
---
bgp_sessions:
  - host: Spine01
    neighbors:
      - Leaf01
      - Leaf02
      - Leaf03
      - Leaf04

  - host: Spine02
    neighbors:
      - Leaf01
      - Leaf02
      - Leaf03
      - Leaf04

  - host: Leaf01
    neighbors:
      - Spine01
      - Spine02

  - host: Leaf02
    neighbors:
      - Spine01
      - Spine02

  - host: Leaf03
    neighbors:
      - Spine01
      - Spine02

  - host: Leaf04
    neighbors:
      - Spine01
      - Spine02
```

![check_bgp_sessions.png](./images/check_bgp_sessions.png)

## Check LLDP links:

I suppose that you use <> for deploy your network. With this tool you can deploy a network based on a YAML file. In this YAML file you will define links between your devices. 

```yaml
links:
  - id: 1
    type: ethernet
    network: bridge
    src: Spine01
    sport: swp1
    dst: Leaf01
    dport: swp1
  - id: 2
    type: ethernet
    network: bridge
    src: Spine01
    sport: swp2
    dst: Leaf02
    dport: swp1
  - id: 3
    type: ethernet
    network: bridge
    src: Spine01
    sport: swp3
    dst: Leaf03
    dport: swp1
  - id: 4
    type: ethernet
    network: bridge
    src: Spine01
    sport: swp4
    dst: Leaf04
    dport: swp1
  - id: 5
    type: ethernet
    network: bridge
    src: Spine02
    sport: swp4
    dst: Leaf04
    dport: swp2
  - id: 6
    type: ethernet
    network: bridge
    src: Spine02
    sport: swp3
    dst: Leaf03
    dport: swp2
  - id: 7
    type: ethernet
    network: bridge
    src: Spine02
    sport: swp2
    dst: Leaf02
    dport: swp2
  - id: 8
    type: ethernet
    network: bridge
    src: Spine02
    sport: swp1
    dst: Leaf01
    dport: swp2
  - id: 9
    network: pnet5
    src:
      - host: Spine01
        port: eth0
        ip_mgmt: 10.0.5.101
        ssh: 22
        nat: 22101
      - host: Spine02
        port: eth0
        ip_mgmt: 10.0.5.102
        ssh: 22
        nat: 22102
      - host: Leaf01
        port: eth0
        ip_mgmt: 10.0.5.201
        ssh: 22
        nat: 22201
      - host: Leaf02
        port: eth0
        ip_mgmt: 10.0.5.202
        ssh: 22
        nat: 22202
      - host: Leaf03
        port: eth0
        ip_mgmt: 10.0.5.203
        ssh: 22
        nat: 22203
      - host: Leaf04
        port: eth0
        ip_mgmt: 10.0.5.204
        ssh: 22
        nat: 22204
    dst: OOB-NETWORK
    ip_eve: 10.0.5.1/24
```

![check_lldp_links.png](./images/check_lldp_links.png)



## Retrieve datas

You can also retrieve informations about your network.

### LLDP Links

## ![get_lldp_links.png](./images/get_lldp_links.png)

![get_lldp_links_cli.png](./images/get_lldp_links_cli.png)

### BGP Sessions

![get_bgp_sessions.png](./images/get_bgp_sessions.png)