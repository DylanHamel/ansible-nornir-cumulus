# Cumulus Network - BGP sessions JSON

If BGP session is UP, there is the neighbor hostname in JSON result

```json
{
    "ipv4 unicast": {
        "as": 65202,
        "bestPath": {
            "multiPathRelax": "true"
        },
        "dynamicPeers": 0,
        "peerCount": 3,
        "peerGroupCount": 1,
        "peerGroupMemory": 64,
        "peerMemory": 59208,
        "peers": {
            "peerlink.4094": {
                "idType": "interface",
                "inq": 0,
                "msgRcvd": 0,
                "msgSent": 0,
                "outq": 0,
                "peerUptime": "never",
                "peerUptimeMsec": 0,
                "prefixReceivedCount": 0,
                "remoteAs": 0,
                "state": "Idle",
                "tableVersion": 0,
                "version": 4
            },
            "swp1": {
                "hostname": "Spine01",						<<<<<<<<<<<<==============
                "idType": "interface",
                "inq": 0,
                "msgRcvd": 12400,
                "msgSent": 12422,
                "outq": 0,
                "peerUptime": "02:11:09",
                "peerUptimeEstablishedEpoch": 1562192230,
                "peerUptimeMsec": 7869000,
                "prefixReceivedCount": 6,
                "remoteAs": 65101,
                "state": "Established",
                "tableVersion": 0,
                "version": 4
            },
            "swp2": {
                "hostname": "Spine02",
                "idType": "interface",
                "inq": 0,
                "msgRcvd": 12397,
                "msgSent": 12418,
                "outq": 0,
                "peerUptime": "02:11:09",
                "peerUptimeEstablishedEpoch": 1562192230,
                "peerUptimeMsec": 7869000,
                "prefixReceivedCount": 6,
                "remoteAs": 65102,
                "state": "Established",
                "tableVersion": 0,
                "version": 4
            }
        },
```



But if the session is down (idle for example), hostname doesn't exist.

```json
{
    "ipv4 unicast": {
        "as": 65201,
        "bestPath": {
            "multiPathRelax": "true"
        },
        "dynamicPeers": 0,
        "peerCount": 3,
        "peerGroupCount": 1,
        "peerGroupMemory": 64,
        "peerMemory": 59208,
        "peers": {
            "peerlink.4094": {
                "idType": "interface",
                "inq": 0,
                "msgRcvd": 0,
                "msgSent": 0,
                "outq": 0,
                "peerUptime": "never",
                "peerUptimeMsec": 0,
                "prefixReceivedCount": 0,
                "remoteAs": 0,
                "state": "Idle",
                "tableVersion": 0,
                "version": 4
            },
            "swp1": {
                "idType": "interface",
                "inq": 0,
                "msgRcvd": 0,
                "msgSent": 0,
                "outq": 0,
                "peerUptime": "never",
                "peerUptimeMsec": 0,
                "prefixReceivedCount": 0,
                "remoteAs": 0,
                "state": "Idle",
                "tableVersion": 0,
                "version": 4
            },
            "swp2": {
                "idType": "interface",
                "inq": 0,
                "msgRcvd": 0,
                "msgSent": 0,
                "outq": 0,
                "peerUptime": "never",
                "peerUptimeMsec": 0,
                "prefixReceivedCount": 0,
                "remoteAs": 0,
                "state": "Idle",
                "tableVersion": 0,
                "version": 4
            }
        },
```

