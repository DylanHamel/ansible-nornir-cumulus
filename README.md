# Cumulus with Ansible & Nornir

![network.png](./nornir/images/network.png)


To deploy this network

## 0. Install Python and Ansible on your machine

   (Jinja2, Nornir, Netmiko, Paramiko, YAML, etc.)


## 1.Download and install EVE-NG :

http://www.eve-ng.net/

Install Cumulus Network OS
Download image on Cumulus Network website :
   
<https://cumulusnetworks.com/products/cumulus-vx/download/>
    
Upload in your EVE-NG VM
   
On your OS (MacOSX for example)

```shell
scp ./cumulus-linux-3.7.5-vx-amd64-qemu.qcow2 root@172.16.194.239:/tmp
```

Connecte on your EVE-NG VM

```shell
ssh -l root 172.16.194.239
```

Create a directory

```shell
mkdir /opt/unetlab/addons/qemu/cumulus-vx-3.7.5
```

Move your qcow2

```shell
mv /tmp/cumulus-linux-3.7.5-vx-amd64-qemu.qcow2 /opt/unetlab/addons/qemu/cumulus-vx-3.7.5/virtioa.qcow2
```

Fix permissions

```shell
/opt/unetlab/wrappers/unl_wrapper -a fixpermissions
```


## 2. Run the following script

```bash
# Download the script
git clone https://gitlab.com/DylanHamel/python-eveng-api

cd python-eveng-api
chmod 777 eveng-api.py 

./eveng-api.py --deploy=/path/to/this/repo/eveng/topology/topology.yml
```

## 3. Push configuration with Ansible

```bash
ansible-playbook -i /path/to/python-eveng-api/tools/ansible/hosts deploy_fabric.yml
```


## 4. Check how you can connecte to the device

```shell
./eveng-api.py --connexion=data-center-cumulus.unl
```

